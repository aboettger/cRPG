import random
from monster import Monster

class Combat:
    enemyTypes = ["skeleton", "bat"]

    def getEnemy():
        return random.choice(Combat.enemyTypes)

    def fightEnemy(enemy):
        if enemy == "skeleton":
            return Monster.Skeleton.xp_value
        elif enemy == "bat":
            return Monster.Bat.xp_value
