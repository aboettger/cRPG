import sys
from player import Player
from monster import Monster
from combat import Combat

class Game:

    def quit():
        sys.exit(0)

    def save():
        file = open("save.txt", "w")
        file.write(Player.name+"\n")
        file.write(Player.gender+"\n")
        file.write(Player.race+"\n")
        file.write(Player.cclass+"\n")
        file.write(Player.level+"\n")
        file.write(str(Player.xp)+"\n")
        file.write(Player.strength+"\n")
        file.write(Player.dexterity+"\n")
        file.write(Player.intellect+"\n")
        file.write(str(Player.hp)+"\n")
        file.write(Player.constitution+"\n")
        file.close()

    def load():
        file = open("save.txt", "r")
        Player.name = file.readline().replace("\n", "")
        Player.gender = file.readline().replace("\n", "")
        Player.race = file.readline().replace("\n", "")
        Player.cclass = file.readline().replace("\n", "")
        Player.level = file.readline().replace("\n", "")
        Player.xp = file.readline().replace("\n", "")
        Player.strength = file.readline().replace("\n", "")
        Player.dexterity = file.readline().replace("\n", "")
        Player.intellect = file.readline().replace("\n", "")
        Player.hp = file.readline().replace("\n", "")
        Player.constitution = file.readline().replace ("\n", "")
        file.close()

    def newCharacter():
        Player.name = input("Name: ")

        check = False
        while check == False:
            temp = input("Gender: (Male, Female) ")
            if temp == "Male":
                Player.gender = temp
                check = True
            elif temp == "Female":
                Player.gender = temp
                check = True
            else:
                print("Please try again")

        check = False
        while check == False:
            temp = input("Race: (Human, Elf, Half-Elf) ")
            if temp == "Human":
                Player.race = temp
                check = True
            elif temp == "Elf":
                Player.race = temp
                check = True
            elif temp == "Half-Elf":
                Player.race = temp
                check = True
            else:
                print("Please try again")

        check = False
        while check == False:
            temp = input("Class: (Warrior, Ranger, Mage) ")
            if temp == "Warrior":
                Player.cclass = temp
                check = True
            elif temp == "Ranger":
                Player.cclass = temp
                check = True
            elif temp == "Mage":
                Player.cclass = temp
                check = True
            else:
                print("Please try again")

        Player.xp = "0"

        if Player.cclass == "Warrior":
            Player.strength = "2"
            Player.dexterity = "1"
            Player.intellect = "1"
            Player.constitution = "3"
        elif Player.cclass == "Ranger":
            Player.strength = "1"
            Player.dexterity = "2"
            Player.intellect = "1"
            Player.constitution = "2"
        elif Player.cclass == "Mage":
            Player.strength = "1"
            Player.dexterity = "1"
            Player.intellect = "2"
            Player.constitution = "1"

        Player.level = "1"

        Player.hp = int(Player.constitution) * 10

    def inventory():
        pass

    def status():
        print("\nName: "+Player.name)
        print("Gender: "+Player.gender)
        print("Race: "+Player.race)
        print("Class: "+Player.cclass)
        print("Level: "+Player.level)
        print("XP: "+str(Player.xp))
        print("HP: "+str(Player.hp))
        print("Strength: "+Player.strength)
        print("Dexterity: "+Player.dexterity)
        print("Intellect: "+Player.intellect)
        print("Constitution: "+Player.constitution)

    def start():
        mode = input('Create "new" or "load" existing character or "exit" Game? ')
        if mode == "new":
            Game.newCharacter()
            Game.save()
            print("Done.")
            Game.main()
        elif mode == "load":
            Game.load()
            print("Character "+Player.name+" loaded")
            Game.main()
        elif mode == "exit":
            Game.quit()
        else:
            print("Try again!")
            Game.start()

    def fight():
        enemy = Combat.getEnemy()
        outcome = Combat.fightEnemy(enemy)
        Player.xp = int(Player.xp) + outcome
        Game.main()

    def main():
        action = input("\nChoose an action: (status, fight, save, exit) ")
        if action == "status":
            Game.status()
            Game.main()
        elif action == "fight":
            Game.fight()
            Game.main()
        elif action == "save":
            Game.save()
            Game.main()
        elif action == "exit":
            Game.save()
            Game.quit()
        else:
            print("Try again!")
            Game.main()

class Start:
    Game.start()
