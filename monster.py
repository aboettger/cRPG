class Monster:

    class Skeleton:
        hp = 10
        attack = 2
        xp_value = 10

    class Bat:
        hp = 5
        attack = 1
        xp_value = 5
